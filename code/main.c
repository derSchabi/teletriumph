#define F_CPU 1000000UL
#define BAUD 9600UL

#include <avr/io.h>
#include <util/delay.h>
#include <util/setbaud.h>

#define NOP asm volatile("nop")
#define RCWAIT for(uint8_t k = 0; k < 32; k++) NOP;

void uart_init(void)
{
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~(_BV(U2X0));
#endif

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);   /* Enable RX and TX */
}

void uart_putchar(char c)
{
    loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
    UDR0 = c;
}

char uart_getchar(void)
{
    loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
    return UDR0;
}

void uart_print(char *str)
{
    while(*str != 0)
    {
        uart_putchar(*str);
        str++;
    }
}


void read_data(uint8_t* data)
{
	PORTA = 1;
	for(uint8_t i = 0; i < 8; i++)
	{
		RCWAIT;
		data[i] = PINC;
		PORTA <<= 1; 
	}
	PORTA = 0;
	PORTB |= (1<<0);
	RCWAIT;
	data[8] = PINC;
    PORTB &= ~(1<<0);
}


int main(void)
{
    uart_init();
    DDRA &= ~(1<<PA0);
    DDRA |= (1<<PA1);
    PORTA &= ~(1<<PA0);
   
    DDRA |= (1<<PA1);
    
    while(1)
	{
        char bla[] = "hallo welt\r\n";

        if(PINA & (1<<PA0)) {
            PORTA |= (1<<PA1);
        } else {
            PORTA &= ~(1<<PA1);
        }
    }
}

