$fn = 20;

difference() {
    cube([27+12,2,8]);
    translate([6+2, -0.1, 1]) union() {
        cube([23,0.6,8]);
        for(i = [0:7]) {
            translate([0.5+2.54+ i * 2.54, 10, 4]) rotate([90, 0, 0]) cylinder(10, 0.9, 0.9);
        }
    }
    
    translate([4, 10, 4]) rotate([90, 0, 0]) cylinder(20, 1.5 , 1.5);
    translate([27+12-4, 10, 4]) rotate([90, 0, 0]) cylinder(20, 1.6 ,1.6);
}

