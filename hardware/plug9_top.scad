$fn = 20;

difference() {
    cube([30.5+12,2,8]);
    translate([6+2, -0.1, 1]) cube([26.5,0.6,8]);

    translate([4, 10, 4]) rotate([90, 0, 0]) cylinder(20, 1.6, 1.6);
    translate([30.5+12-4, 10, 4]) rotate([90, 0, 0]) cylinder(20, 1.6, 1.6);
}
