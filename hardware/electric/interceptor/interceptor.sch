EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FPGA_Lattice:ICE40HX1K-TQ144 U?
U 1 1 5D71A554
P 6850 4900
F 0 "U?" H 7230 5028 50  0000 L CNN
F 1 "ICE40HX1K-TQ144" H 7230 4937 50  0000 L CNN
F 2 "Package_QFP:TQFP-144_20x20mm_P0.5mm" H 6850 3450 50  0001 C CNN
F 3 "http://www.latticesemi.com/Products/FPGAandCPLD/iCE40" H 6000 6300 50  0001 C CNN
	1    6850 4900
	1    0    0    -1  
$EndComp
$Comp
L FPGA_Lattice:ICE40HX1K-TQ144 U?
U 2 1 5D71D24B
P 4150 2300
F 0 "U?" H 4530 2428 50  0000 L CNN
F 1 "ICE40HX1K-TQ144" H 4530 2337 50  0000 L CNN
F 2 "Package_QFP:TQFP-144_20x20mm_P0.5mm" H 4150 850 50  0001 C CNN
F 3 "http://www.latticesemi.com/Products/FPGAandCPLD/iCE40" H 3300 3700 50  0001 C CNN
	2    4150 2300
	1    0    0    -1  
$EndComp
$Comp
L FPGA_Lattice:ICE40HX1K-TQ144 U?
U 3 1 5D7265F6
P 6800 8100
F 0 "U?" H 7180 8228 50  0000 L CNN
F 1 "ICE40HX1K-TQ144" H 7180 8137 50  0000 L CNN
F 2 "Package_QFP:TQFP-144_20x20mm_P0.5mm" H 6800 6650 50  0001 C CNN
F 3 "http://www.latticesemi.com/Products/FPGAandCPLD/iCE40" H 5950 9500 50  0001 C CNN
	3    6800 8100
	1    0    0    -1  
$EndComp
$Comp
L FPGA_Lattice:ICE40HX1K-TQ144 U?
U 4 1 5D7288AA
P 4000 5550
F 0 "U?" H 4380 5678 50  0000 L CNN
F 1 "ICE40HX1K-TQ144" H 4380 5587 50  0000 L CNN
F 2 "Package_QFP:TQFP-144_20x20mm_P0.5mm" H 4000 4100 50  0001 C CNN
F 3 "http://www.latticesemi.com/Products/FPGAandCPLD/iCE40" H 3150 6950 50  0001 C CNN
	4    4000 5550
	1    0    0    -1  
$EndComp
$Comp
L FPGA_Lattice:ICE40HX1K-TQ144 U?
U 5 1 5D72AAD7
P 6700 1750
F 0 "U?" H 7344 1796 50  0000 L CNN
F 1 "ICE40HX1K-TQ144" H 7344 1705 50  0000 L CNN
F 2 "Package_QFP:TQFP-144_20x20mm_P0.5mm" H 6700 300 50  0001 C CNN
F 3 "http://www.latticesemi.com/Products/FPGAandCPLD/iCE40" H 5850 3150 50  0001 C CNN
	5    6700 1750
	1    0    0    -1  
$EndComp
$Comp
L localfoo:SN74LVC16T245DLR U?
U 1 1 5D7863B9
P 10150 1650
F 0 "U?" H 10150 -1889 50  0000 C CNN
F 1 "SN74LVC16T245DLR" H 10150 -1980 50  0000 C CNN
F 2 "" H 9800 2800 50  0001 C CNN
F 3 "" H 9800 2800 50  0001 C CNN
	1    10150 1650
	1    0    0    -1  
$EndComp
$Comp
L localfoo:SN74LVC16T245DLR U?
U 1 1 5D793AF9
P 10000 6450
F 0 "U?" H 10000 2911 50  0000 C CNN
F 1 "SN74LVC16T245DLR" H 10000 2820 50  0000 C CNN
F 2 "" H 9650 7600 50  0001 C CNN
F 3 "" H 9650 7600 50  0001 C CNN
	1    10000 6450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
