TeleTriumph
===========

Make a Triumph-Adler Gabriele PFS into a Teletyper.

You can find the datasheet of the used ATmega644 20P [here](http://ww1.microchip.com/downloads/en/DeviceDoc/doc2593.pdf).
